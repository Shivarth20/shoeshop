$(document).ready(function() {

  $('.users-table').DataTable({
    processing: true,
    serverSide: true,
    autoWidth : false,
    ajax: {
      url : "/admin/users",
    },
    columns : [
      {
        data : 'id',
        name : 'id'
      },
      {
        data : 'name',
        name : 'name'
      },
      {
        data: 'image_url', 
        name: 'image_url',
        orderable: false,
        searchable: false,
        render: function( data, type, full, meta ) {
            return "<img src='"+data+"'>";
         }
      },
      {
        data : 'email',
        name : 'email'
      },
      {
        data : 'mobile',
        name : 'mobile'
      },
      {
        data : 'role_name',
        name : 'role_name'
      },
      {
        data : 'created_at',
        name : 'created_at'
      },
      {
        data : 'updated_at',
        name : 'updated_at'
      },
      {
        data : 'action',
        name : 'action',
        orderable: false,
        searchable: false
      }
    ]
  });

  $('.products-table').DataTable({
  order     : [ 1, "asc" ],  
  processing: true,
  serverSide: true,
  autoWidth : false,
  ajax: {
    url : "/admin/products",
  },
  columns : [
    {
      data : 'id',
      name : 'id'
    },
    {
      data : 'name',
      name : 'name'
    },
    {
      data : 'parent_category',
      name : 'parent_category'
    },
    {
      data : 'description',
      name : 'description'
    },
    {
      data: 'image_url', 
      name: 'image_url',
      orderable: false,
      searchable: false,
      render: function( data, type, full, meta ) {
          return "<img src='"+data+"'>";
       }
    },
    
    {
      data : 'created_at',
      name : 'created_at'
    },
    {
      data : 'updated_at',
      name : 'updated_at'
    },
    {
      data : 'action',
      name : 'action',
      orderable: false,
      searchable: false
    }
  ]
  });        

  $('.categories-table').DataTable({
    order     : [ 1, "asc" ],
    processing: true,
    serverSide: true,
    autoWidth : false,
    
    ajax: {
      url : "/admin/categories",
    },
    columns : [
      {
        data : 'id',
        name : 'id'
      },
      {
        data : 'name',
        name : 'name'
      },
      { data: 'image_url', 
        name: 'image_url',
        orderable: false,
        searchable: false,
      render: function( data, type, full, meta ) {
          return "<img src='"+data+"'>";
        }
      },
      {
        data : 'parent_id',
        name : 'parent_id'
      },
      {
        data : 'created_at',
        name : 'created_at'
      },
      {
        data : 'updated_at',
        name : 'updated_at'
      },
      {
        data : 'action',
        name : 'action',
        orderable: false,
        searchable: false
    
      }
    ]
  });          

  $(document)
    
    .on("click", ".delete",function() {

      var data_type = $(this).attr('data-type'),
          id        = $(this).attr('data-id'),
          name      = $(this).attr('data-name'), 
          url       = '';


      switch(data_type) {
        
        case 'product':
          url = '/admin/product/delete/';
          break;

        case 'user':
          url = '/admin/user/delete/';
          break;
        
        case 'category':
          url = '/admin/category/delete/';
          break;  
      
      }

        if(url) {
          $.confirm({
          title : "Delete "+ data_type,  
          content: 'Are you sure you want to delete the '+ data_type +' "'+ name +'" ?',
          buttons: {
            Yes :{
              btnClass: 'btn-success',
              action: function(){
                return $.ajax({
                    url: url,
                    data : {
                      "_token": "{{ csrf_token() }}",
                      "id"     : id  
                    },
                    method: 'POST',
                  }).done(function (response) { 
                    $.alert('Product Deleted!');
                    $(this).parent().remove();
                }).fail(function(){
                  $.alert('Something went wrong.');
                });
              }
            },
            No: function () {
                $.alert('Canceled!');
            },
          }
      });
    } else {
      $.alert('Invalid Request!');
    }

    });
});