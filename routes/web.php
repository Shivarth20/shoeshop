<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auth::routes();

Route::get('/','HomeController@index');

//Route::view('/login', 'shoeshop.login');
Route::view('/register', 'shoeshop.register');
Route::view('/contact', 'shoeshop.contact');
Route::view('/cart', 'shoeshop.cart');

Route::post('/send_mail', 'SendmailController@send');

Route::get('/view/{category_name}','CategoryController@show');



// Login Route
Route::view('/admin/login', 'admin.login');

Route::post('/admin/login-form', 'UserController@adminLogin');

Route::middleware(['admin'])->group(function () {

  // Dashboard Controller
  Route::get('/admin', 'AdminController@dashboard');
  Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin-dashboard');


  // User Controller
  Route::get('/admin/users', 'UserController@index');
  Route::get('/admin/user/edit/{id}','UserController@edit');
  Route::post('/admin/user/update','UserController@update');
  Route::post('/admin/user/delete/{id}','UserController@delete');

  // Product Controller
Route::get('/admin/products', 'ProductController@index');
  Route::get('/admin/product/edit/{id}','ProductController@edit');
  Route::get('/admin/product/add','ProductController@add');
  Route::post('/admin/product/update','ProductController@update');
  Route::post('/admin/product/insert','ProductController@insert');
  Route::post('/admin/product/delete','ProductController@delete');

  // Category Controller
  Route::get('/admin/categories', 'CategoryController@index');
  Route::get('/admin/category/edit/{id}','CategoryController@edit');
  Route::get('/admin/category/add','CategoryController@add');
  Route::post('/admin/category/update','CategoryController@update');
  Route::post('/admin/category/insert','CategoryController@insert');
  Route::post('/admin/category/delete','CategoryController@delete');

    // Order Controller
    Route::get('/admin/orders', 'OrderController@index');


  // Logout Controller
  Route::get('/admin/logout', function(){
    Auth::logout();
    return Redirect::to('/admin/login');
  });

});

Route::post('/login_user', 'UsersController@logout');
