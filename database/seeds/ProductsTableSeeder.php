<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Carbon\Carbon;



class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

$products_lists = [
    [
        'name' => 'Hans Runner Shoe1', 
        'category_id' => App\Models\Category::select('id')->where('name', '=', 'Running Looks')->first()->id, 
        'slug' => Str::slug('Hans Runner Shoe'),
        'description' => 'This is a genuine product of Reebok India Company. The product comes with a standard brand warranty of 90 days.',
    ]

];

DB::table('products')->create($products_lists);
}
}
