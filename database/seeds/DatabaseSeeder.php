<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // for now we have only defined one seeder

         $this->call(ProductsableSeeder::class);
    }
}
