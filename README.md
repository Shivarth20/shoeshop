## Objective

Create an e-commerce system using [Laravel v6](https://laravel.com/docs/6.x).

## Functionality

1. Admin should have the ability to manage categories, products, product attributes, orders, users in the system.
2. All database tables to be added through migrations.
3. Use Twilio to send a welcome sms to the user when user registers.
4. Guest users can add/delete/update cart items but only logged in users can place orders.
5. Integrate stripe payment gateway to place orders.
6. Add proper validations (clientside + serverside) wherever applicable.
7. Main menu items will be Home, Shoes, Cart, Checkout, My Account, Contact Us.
8. Emails should be sent upon different actions like registration, order purchase, contact us.
9. My Account is the dashboard on site end.
10. Listing page should have filter functionality.


## Notes

1. For now consider only shoes as a product collection.
2. For managing media, please use [spatie media library](https://github.com/spatie/laravel-medialibrary) package.
3. For managing table, please use [laravel DataTables](https://yajrabox.com/docs/laravel-datatables/master) package.
4. Admin end to be developed using the [AdminLTE](https://adminlte.io/themes/AdminLTE/index.html) template.
5. Site end to be developed using the [Shoes store](http://themehunt.com/item/1526528-time-watch-multipurpose-responsive-ecommerce-html-template-with-4-layout/preview) theme.