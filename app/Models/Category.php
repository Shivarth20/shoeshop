<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Category extends Model implements HasMedia
{
    use HasMediaTrait;

    //
    protected $fillable = [
        'parent_id', 'name', 'slug'
    ];

    protected $appends = [
        'image_url',
        'parent_category'
    ];

    public function getNameAttribute($name)
    {
        return ucfirst($name);
    }

    public function getImageUrlAttribute()
    {
        $image_url = $this->getFirstMediaUrl();
        if(empty($image_url)) {

            // Set default image if image not present
            $image_url = '/image/image-not-available.png';
        }
        return $image_url;

    }


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('category')
              ->width(380)
              ->height(250)
              ->sharpen(10);
    }

    public static function getDropdownOptions() {
        
        $categories = array();

        $parent_categories = Category::select('id', 'name')->where ('parent_id', '<>', 0)->get();
        
        foreach($parent_categories as $category_value) {
           $categories[$category_value->id] = $category_value->name;    
        }
        return $categories;
     
    }

    public static function getParentSlug() {
        
        $categories = array();

        $parent_categories = Category::select('slug', 'name')->where ('parent_id', '<>', 0)->get();
        
        foreach($parent_categories as $category_value) {
           $categories[$category_value->slug] = $category_value->name;    
        }
        return $categories;
     
    }

    public static function getParentCategories() {
        
        $categories = array();

        return Category::select('id', 'name')->where ('parent_id', '=', 0)->get(); 
    }

    // Get Active Categories count
    public static function activeCategoriesCount()
    {
        return Category::count();
    }

    // set Parent category value
    public function getParentCategoryAttribute()
    {
        return $this->parent_id;
    }

}
