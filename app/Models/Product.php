<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use App\Models\Category as CategoryModel;


class Product extends Model implements HasMedia
{
    //

    use SoftDeletes;
    use HasMediaTrait;

    protected $fillable = [
        'name', 'slug', 'slug', 'description', 'category_id'
    ];

    protected $appends = [
        'image_url',
        'parent_category'
    ];

    public function getImageUrlAttribute()
    {
        $image_url = $this->getFirstMediaUrl();
        if(empty($image_url)) {

            // Set default image if image not present
            $image_url = '/image/image-not-available.png';
        }
        return $image_url;

    }

    public function getParentCategoryAttribute()
    {
        return $this->categories->name;
    }

    public function categories()
    {
        return $this->hasOne(CategoryModel::class, 'id', 'category_id');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('product')
              ->width(600)
              ->height(632)
              ->sharpen(10);
    }

    // Get Active Products count
    public static function activeProductsCount()
    {
        return Product::count();
    }
}
