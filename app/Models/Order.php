<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    //
    //use SoftDeletes;

    // Get Active Orders count
    public static function activeOrdersCount()
    {
        return Order::count();
    }
}
