<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use SoftDeletes;
    use HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'image_url',
        'role_name'
    ];

    public function getImageUrlAttribute()
    {
        $image_url = $this->getFirstMediaUrl();
        if(empty($image_url)) {

            // Set default image if image not present
            $image_url = '/image/avatar.png';
        }
        return $image_url;

    }


    public static function getTableName()
    {
        return (new self())->getTable();
    }

    public function parentable()
    {
        return $this->morphTo();
    }

    public function getNameAttribute($name)
    {
        return ucfirst($name);
    }

    // set role name value
    public function getRoleNameAttribute()
    {
        return $this->role_id == 1 ? 'Admin': 'Customer';
    }

    // Get Active users count
    public static function activeUsersCount()
    {
        return User::count();
    }

}
