<?php

namespace App\Http\Controllers;

use App\user_model;
use App\User;

class IndexController extends Controller
{
    public function home()
    {
        $products=user_model::all();

        $products = user_model::where('id', '<' , 10)->get();
               
        $products = user_model::select('country_name')->get();

        echo User::getTableName();
       
        return view('users', [
            'products' => $products
        ]);
    }
}

#find - using primary key
#first - limit 0,1
#firstOr - limit 0,1 else callabele function

# update
# $flight = App\Flight::find(1);
# $flight->name = 'New Flight Name';
# $flight->save();

# update multiple

## user_model::where('active', 1)
##->where('destination', 'San Diego')
##->update(['delayed' => 1]);

##Examining Attribute Changes
## refresh mass-assignment vulnerability
# fillable - to allow only given column name data to be inserted
# create - insert ; fill,update for update
#guarded - blocklist not needed item
# softDeletes - flag as delete

## different model event- retrieved, creating, created, updating, updated, saving, saved, deleting, deleted, restoring, restored.


## one to one
## hasOne(model, foreign key, primary key)
## belongsTo(REVERSE OF ABOVE)

## one to many
## 