<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Yajra\DataTables\DataTables;
use DataTable;


class UserController extends Controller
{

    public function fetch()
     {

        $users_details = User::withTrashed()->get();

        $user_avatar = array();

        foreach($users_details as $users_detail) {

           $user_image = User::withTrashed()->find($users_detail->id)->getFirstMediaUrl();
           if(!empty($user_image)) {
                $user_avatar[$users_detail->id] = $user_image;
           }
        }
        
        return view('admin.user.index',[
            'users_details' => $users_details,
            'user_avatar'   => $user_avatar
          ]);
   
    }

    public function index (Request $request) 
    {

        if ($request->ajax()) {
            return Datatables::of(User::withTrashed()->get())
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="/admin/user/edit/'.$row->id.'" class="action-button btn btn-primary btn-sm"><i class = "fa fa-edit"></i></a>';
                        $btn .= '<button data-type = "user" data-name = "'.$row->name.'" data-id = "'.$row->id.'" class="delete action-button btn btn-danger btn-sm">
                                    <i class = "fa fa-trash"></i>
                                </button>';
                      return $btn;
                    })
                    ->make(true);
        }

        return view('admin.user.index');
    }



     public function edit($id) 
     {

        $user_details = User::withTrashed()->find($id);

        if(empty($user_details)) {
            return redirect('/admin/users')
                ->with('error', 'Invalid User Id');
        }


        return view('admin.user.edit',[
            'user_details'  => $user_details,
          ]);
     }

     public function update(Request $request) 
     {

        $this->validate($request, [
            'email'   => 'required|string|email',
            'name'    => 'required',
            'mobile'  => 'required',
            'role-id' => 'required',
            'id'      => 'required',
        ]);

        $input_data=$request->all();

        User::withTrashed()->where('id', $input_data['id'])
             ->update(
              [
                  'email'   => $input_data['email'],
                  'name'    => $input_data['name'],
                  'mobile'  => $input_data['mobile'],
                  'role_id' => $input_data['role-id']
              ]
              );
        
        return back()->with('success','User Updated');
     }

    public function delete(Request $request) 
    {

        $input_data=$request->all();

        User::where('id', $input_data['id'])->delete();

    } 

    public function register()
    {    
        return view('register');
    }

    public function adminLogin(Request $request)
    {
            
        $this->validate($request, [
                'email'  =>'required|string|email',
                'password'  =>'required'
                ]);
            
            $input_data=$request->all();
           
            if(Auth::attempt(['email'=>$input_data['email'],'password'=>$input_data['password'],'deleted_at'=> null])){
                return redirect('/admin');
            } else {
              return back()->with('error','Invalid Username or Password');
            }
    }

}