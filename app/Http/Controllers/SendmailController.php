<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEnquiry;


class SendmailController extends Controller
{

    public function send(Request $request)
    {
        $this->validate($request, [
                'name'   =>'required|string|max:255',
                'email'  =>'required|string|email',
                'enquiry'=>'required|min:6',
        ]);

        $data = array(
            'name'    => $request->name,
            'enquiry' => $request->enquiry
        );

        Mail::to('mfs.shivarth@gmail.com')->send(new SendEnquiry($data));

        return back()->with('sucess','Thanks For contacting Us!');
    
    }

}
