<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Category;
use App\Models\Product;
use App\Models\Order;


class AdminController extends Controller
{

    public function dashboard()
    {        
        return view('admin.dashboard', [
            'orders_count'     => Order::activeOrdersCount(),
            'users_count'      => User::activeUsersCount(),
            'categories_count' => Category::activeCategoriesCount(),
            'products_count'   => Product::activeProductsCount(),
        ]
    );
    }

}
