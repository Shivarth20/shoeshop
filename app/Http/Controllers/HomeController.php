<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;


class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $parent_category = Category::getParentSlug();

        return view('shoeshop.home', [
            'parent_category' => $parent_category,
        ]);
    }
}
