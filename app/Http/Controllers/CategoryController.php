<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Category;
use DataTable;
use Yajra\DataTables\DataTables;
use Auth;


class CategoryController extends Controller
{
    public function index (Request $request) 
    {

        if ($request->ajax()) {
            return Datatables::of(Category::all())
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<a href="/admin/category/edit/'.$row->id.'" class="action-button btn btn-primary btn-sm"><i class = "fa fa-edit"></i></a>';
                           $btn .= '<button data-type = "category" data-name = "'.$row->name.'" data-id = "'.$row->id.'" class="delete action-button btn btn-danger btn-sm"><i class = "fa fa-trash"></i></button>';
                            return $btn;
                    })
                    ->make(true);
        }

        return view('admin.category.index');
    }

    // Display add page
    public function add() 
    {

        $main_categories = Category::getParentCategories();

        return view('admin.category.add',[
            'main_categories' => $main_categories,
        ]);
    }

    // Display edit page
    public function edit($id) 
    {

        $category_details = Category::find($id);

        if(empty($category_details)) {
            return redirect('/admin/categories')
                ->with('error', 'Invalid Category Id');
        }

        return view('admin.category.edit',[
            'category_details' => $category_details,
            'main_categories'  => Category::getParentCategories()
          ]);
    }

    // Update Category
    public function update(Request $request) 
    {

        $input_data=$request->all();

        $this->validate($request, [
            'name' => 'required|unique:categories,name,'.$input_data['id']
        ]);

        $category_form = Category::where('id', '=', $input_data['id'])
                        ->update(
                            [
                            'name' => $input_data['name'],
                            'slug' => Str::slug($input_data['name']),
                            ]);
        
        if(!empty($input_data['category_image'])) {
                        
            Category::find($input_data['id'])->clearMediaCollection()->addMediaFromRequest('category_image')
            ->toMediaCollection();            
        }

        return back()->with('success','Category Updated');
    
    }

    // Insert new category
    public function insert(Request $request) 
    {

        $this->validate($request, [
            'name'           => 'required|unique:categories',
            'category_image' => 'required',
        ]);

        $input_data=$request->all();

        $category_form = Category::Create(
                    [
                        'name'      => $input_data['name'],
                        'slug'      => Str::slug($input_data['name']),
                        'parent_id' => $input_data['parent_id'],
                    ]
                );

        $category_form->addMediaFromRequest('category_image')
        ->toMediaCollection();
   

        return back()->with('success','Category Added');
    }

    // Delete request
    public function delete(Request $request) 
    {

        $input_data=$request->all();

        Category::where('id', $input_data['id'])->delete();

    }
    
    // Category slug page
    public function show($category_name) 
    {

        $category_details = Category::where('slug', $category_name)->get();
        $parent_category  = Category::getParentSlug();

        return view('shoeshop.category',[
            'category_details' => $category_details,
            'parent_category'  => $parent_category,
          ]);

    }

}
