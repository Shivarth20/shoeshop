<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Product;
use App\Models\Category;
use Yajra\DataTables\DataTables;



class ProductController extends Controller
{
 
    public function index (Request $request) 
    {

        if ($request->ajax()) {
            
            return Datatables::of(Product::withTrashed()->get())
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="/admin/product/edit/'.$row->id.'" class="action-button btn btn-primary btn-sm"><i class = "fa fa-edit"></i></a>';
                        $btn .= '<button data-type = "product" data-name = "'.$row->name.'" data-id = "'.$row->id.'" class="delete action-button btn btn-danger btn-sm"><i class = "fa fa-trash"></i></button>';
                        return $btn;
                    })
                    ->make(true);
        }

        return view('admin.product.index');
    }

    public function edit($id) 
    {

        $product_details = Product::withTrashed()->find($id);

        if(empty($product_details)) {
            return redirect('/admin/products')
                ->with('error', 'Invalid Product Id');
        }

        return view('admin.product.edit',[
            'product_details'  => $product_details,
            'parent_categories'=> Category::getDropdownOptions(),
          ]);

    }

    public function update(Request $request) 
    {

        $this->validate($request, [
            'name' => 'required',
        ]);

        $input_data=$request->all();

        Product::withTrashed()->where('id', $input_data['id'])
                ->update(
                [
                    'name'        => $input_data['name'],
                    'slug'        => Str::slug($input_data['name']),
                    'category_id' => $input_data['parent_category'],
                    'description' => $input_data['product_description']
                ]);

        return back()->with('success','Product Updated');
    
    }


    public function add() 
    {

        $parent_categories = Category::getDropdownOptions();

        return view('admin.product.add',[
            'parent_categories'=> $parent_categories
          ]);

    }

    public function insert(Request $request) 
    {

        $this->validate($request, [
            'name'                => 'required|unique:products',
            'parent_category'     => 'required',
            'product_description' => 'required',
            'product_image'       => 'required',

        ]);

        $input_data = $request->all();

        $product_form = Product::Create(
                    [
                        'name'        => $input_data['name'],
                        'slug'        => Str::slug($input_data['name']),
                        'category_id' => $input_data['parent_category'],
                        'description' => $input_data['product_description']                    ]
                );

        $product_form->addMediaFromRequest('product_image')
        ->toMediaCollection();

        return back()->with('success','Product Added');
    }

    public function delete(Request $request) 
    {

        $input_data=$request->all();

        Product::where('id', $input_data['id'])->delete();

    }

}
