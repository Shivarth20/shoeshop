@extends('layouts.shoeshop')

@section('content')

@include('shoeshop.partials.breadcrumb')

<div id="column-left" class="col-sm-3 hidden-xs column-left">
    <div class="column-block">
        <div class="columnblock-title">Categories</div>
            <div class="category_block">
            <ul class="box-category treeview-list treeview collapsable">
            @foreach($parent_category as $category_slug => $category_name)
            <a href = "/{{$category_slug}}">
                <li>{{$category_name}}</li>
            </a>
            @endforeach
            </ul>
            </div>
    </div>
</div>
@endsection
