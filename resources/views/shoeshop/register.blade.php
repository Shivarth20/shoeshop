@extends('layouts.shoeshop')
@section('content')
@include('shoeshop.partials.breadcrumb')

    <div class = "col-sm-3">
        <h1>Register section</h1>
        <hr>

    </div>
    <div class = "col-sm-9">
        <div class = "row">
            <h2>Register Account</h2>
            <p>If you already have an account with us, please login at the <a href = "/login">login</a> page.</p>
        </div>
        <form action="{{url('/register_user')}}" method="post" class="form-horizontal register-form">
            <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>Name:</label>
                    </div>
                    <div class = "col-sm-10">
                        <input type="text" placeholder="Name" name="name">
                        <span class="text-danger">{{$errors->first('name')}}</span>
                    </div>      
                </div>

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>Email :</label>
                    </div>

                    <div class = "col-sm-10">
                        <input type="email" placeholder="Email Address" name="email">
                        <span class="text-danger">{{$errors->first('email')}}</span>
                    </div>
                </div>

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>Phone :</label>
                    </div>
                    <div class = "col-sm-10">
                        <input type="phone" placeholder="Phone Number" name="phone">
                        <span class="text-danger">{{$errors->first('email')}}</span>
                    </div>
                </div>

                <div class = "row">
                    <div>
                    <p> Your Address</p>
                    <hr>
                    </div>
                </div>

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>Address :</label>
                    </div>
                    <div class = "col-sm-10">
                        <input type="text" placeholder="Address" name="user_address" />
                        <span class="text-danger">{{$errors->first('user_address')}}</span>
                    </div>   
                </div>

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>city :</label>
                    </div>
                    <div class = "col-sm-10">
                        <input type="text" placeholder="City" name="user_city" />
                        <span class="text-danger">{{$errors->first('user_city')}}</span>
                    </div>   
                </div>

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>Post code :</label>
                    </div>
                    <div class = "col-sm-10">
                        <input type="text" placeholder="Post code" name="post_code" />
                        <span class="text-danger">{{$errors->first('post_code')}}</span>
                    </div>   
                </div>

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>Country :</label>
                    </div>
                    <div class = "col-sm-10">
                        <input type="text" placeholder="Country" name="user_country" />
                        <span class="text-danger">{{$errors->first('user_country')}}</span>
                    </div>   
                </div>
                
                <div class = "row">
                    <div class = "col-sm-2">
                        <label>State :</label>
                    </div>
                    <div class = "col-sm-10">
                        <input type="text" placeholder="State" name="user_state"/>
                        <span class="text-danger">{{$errors->first('user_state')}}</span>
                    </div>   
                </div>

                <p>Your Password</p>
                <hr>

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>Password :</label>
                    </div>
                    <div class = "col-sm-10">
                        <input type="password" placeholder="Password" name="password" value="{{old('password')}}"/>
                        <span class="text-danger">{{$errors->first('password')}}</span>
                    </div>
                </div>

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>Confirm Password :</label>
                    </div>
                    <div class = "col-sm-10">
                        <input type="password" placeholder="Confirm Password" name="password_confirmation"/>
                        <span class="text-danger">{{$errors->first('password_confirmation')}}</span>
                    </div>   
                </div>

                <div class = "row">
                    <div class = "col-sm-2">
                    </div>
                    <div class = "col-sm-10">
                        <button type="submit" class="btn btn-primary register-button">Register Now</button>
                    </div>
                </div>
        </form>

    </div>
@endsection
