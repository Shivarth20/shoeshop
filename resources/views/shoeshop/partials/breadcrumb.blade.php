    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i>Home</a></li>
        @foreach(Request::segments() as $segment)
        <li>
            <a href="{{$segment}}">{{ucfirst($segment) }}</a>
        </li>
        @endforeach
    </ol>
