<nav id="menu" class="navbar">
    <div class="nav-inner">
        <div class="navbar-header"><span id="category" class="visible-xs">Categories</span>
            <button type="button" class="btn btn-navbar navbar-toggle" ><i class="fa fa-bars"></i></button>
        </div>
        <div class="navbar-collapse">
            <ul class="main-navigation">
            <li><a href="index.html"   class="parent">Home</a> </li>
            <li><a href="#" class="active parent">Shoes</a>
                <ul>
                    @foreach($parent_category as $category_slug => $category_name)
                    <li>
                        <a href="/view/{{$category_slug}}">{{$category_name}}</a>
                    </li>
                    @endforeach
                </ul>
            </li>
            <li><a href="/contact" >Contact Us</a> </li>
            </ul>
        </div>
    </div>
</nav>