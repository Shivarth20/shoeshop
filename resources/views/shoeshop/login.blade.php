@extends('layouts.shoeshop')
@section('content')
@include('shoeshop.partials.breadcrumb')

<div class = "row">
    <div class = "col-sm-3">
    <h1>Login section</h1>
    <hr>
    </div>
    <div class = "col-sm-9">
        <div class = "col-sm-6 register-section">
            <h2>New Customer</h2>
            <h3>Register Account</h3>
            <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
            <a href = "/register"><button class="btn btn-primary"> Register</button></a>
        </div>

        <div class = "col-sm-5 login-block">
            <h2>Login</h2>
            <div class = "row">
                <div><label>Email :</label></div>
                <input type="email" placeholder="Email Address" name="email">
            </div>
            <div class = "row">
                <div><label>Password :</label></div>
                <input type="email" placeholder="Password" name="password">
                </div>
                <button type="submit" class="btn btn-primary login-button">Login</button>
            </div>
        </div>
</div>
@endsection
