@extends('layouts.shoeshop')
@section('content')
@include('shoeshop.partials.slider')

<div class="container">
    <div class="row">

        <div class = "banner row">
            <div class = "col-sm-4">
                <img src = "/image/subbanner1.jpg">
            </div>
            <div class = "col-sm-4">
                <img src = "/image/subbanner2.jpg">
            </div>
            <div class = "col-sm-4">
                <img src = "/image/subbanner3.jpg">
            </div>
        </div>

        <div id="tabs" class="customtab-wrapper">
        <ul class="customtab-inner">
            <li class="tab"><a href="#tab-latest" class="">Latest Product</a></li>
            <li class="tab"><a href="#tab-special" class="selected">Special Product</a></li>
            <li class="tab"><a href="#tab-bestseller">Bestseller Items</a></li>
        </ul>
        </div>

        <div class = "row shoe-category">
            <div class = "col-sm-3">
                <img src = "/image/product1.jpg">
                <div class = "description">Office shoe</div>
                <div class = "other-details">
                    <div class = "row">
                    <div class = "col-sm-4">
                        <span class = "price"><b>$12.00</b></span>
                    </div> 
                    </div>
                </div>
            </div>
            <div class = "col-sm-3">
                <img src = "/image/product2.jpg">
            
                <div class = "description">Office shoe</div>
                <div class = "other-details">
                    <div class = "row">
                    <div class = "col-sm-4">
                        <span class = "price"><b>$312.00</b></span>
                    </div> 
                    </div>
                </div>
            </div>
            
            <div class = "col-sm-3">
                <img src = "/image/product3.jpg">
            
                <div class = "description">Office shoe</div>
                <div class = "other-details">
                    <div class = "row">
                    <div class = "col-sm-4">
                        <span class = "price"><b>$124.00</b></span>
                    </div> 
                    </div>
                </div>
            </div>

            <div class = "col-sm-3">
                <img src = "/image/product4.jpg">
                
                <div class = "description">Office shoe</div>
                <div class = "other-details">
                    <div class = "row">
                    <div class = "col-sm-4">
                        <span class = "price"><b>$121.00</b></span>
                    </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection