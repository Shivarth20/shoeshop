@extends('layouts.shoeshop')
@section('content')
@include('shoeshop.partials.breadcrumb')

<div class = "row">
    <div class = "col-sm-3">
        <h1>Login section</h1>
        <hr>
    </div>
    
    <div class = "col-sm-9">
        <div class = "row">
            @if($message = Session::get('message'))
                {{$message}}
            @endif
            <h2>Contact Form</h2>
            <form method = "post" action="{{url('/send_mail')}}" class = "contact-form">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class = "row">
                    <div class = "col-sm-2">
                        <label>Your Name :</label>
                    </div>

                    <div class = "col-sm-10">
                        <input type="text" placeholder="Your Name" name="name" value="{{old('name')}}">
                        <span class="text-danger">{{$errors->first('name')}}</span>
                    </div>

                </div>

                <div class = "row">
                    
                    <div class = "col-sm-2">
                        <label>Email :</label>
                    </div>
                    
                    <div class = "col-sm-10">
                        <input type="text" placeholder="Email" name="email" value="{{old('email')}}">
                        <span class="text-danger">{{$errors->first('email')}}</span>
                    </div>

                </div>

                <div class = "row">
                    
                    <div class = "col-sm-2">
                        <label>Enquiry :</label>
                    </div>
                    
                    <div class = "col-sm-10">
                        <textarea rows = "10" placeholder="Enquiry" name="enquiry" value="{{old('enquiry')}}"></textarea>
                        <span class="text-danger">{{$errors->first('enquiry')}}</span>
                    </div>
                
                </div>

                <button class = "btn btn-primary contact-button">Submit</button>

            </form>

            </div>

        </div>

    </div>

</div>

@endsection