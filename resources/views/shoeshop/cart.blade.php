@extends('layouts.shoeshop')
@section('content')
@include('shoeshop.partials.breadcrumb')
<div class = "row">
    <div class = "col-sm-3">

    </div>
    <div class = "col-sm-9">
        <div class = "row">
            <h2>Shopping Cart</h2>
            <form enctype="multipart/form-data" method="post" action="#">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-center">Image</td>
                <td class="text-left">Product Name</td>
                <td class="text-left">Model</td>
                <td class="text-left">Quantity</td>
                <td class="text-right">Unit Price</td>
                <td class="text-right">Total</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="text-center"><a href="/product.html"><img class="img-thumbnail" title="women's New Wine is an alcoholic" alt="women's New Wine is an alcoholic" src="image/product7.jpg"></a></td>
                <td class="text-left"><a href="/product">women's New Wine is an alcoholic</a></td>
                <td class="text-left">product 11</td>
                <td class="text-left"><div style="max-width: 200px;" class="input-group btn-block">
                    <input type="text" class="form-control quantity" size="1" value="1" name="quantity">
                    <span class="input-group-btn">
                    <button class="btn btn-primary" title="" data-toggle="tooltip" type="submit" data-original-title="Update"><i class="fa fa-refresh"></i></button>
                    <button class="btn btn-danger" title="" data-toggle="tooltip" type="button" data-original-title="Remove"><i class="fa fa-times-circle"></i></button>
                    </span></div></td>
                <td class="text-right">$254.00</td>
                <td class="text-right">$254.00</td>
              </tr>
            </tbody>
          </table>
        </div>
      </form>
        </div>
    </div>
</div>
@endsection