@extends('layouts.admin')

@section('content-header')
  <div class="row">

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">   
        <div class="inner">
          <h3>{{$orders_count}}</h3>
          <p>Orders</p>
        </div>
        <div class="icon">
          <i class="fa fa-cart-arrow-down"></i>
        </div>
        <a href="/admin/orders" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{$categories_count}}</h3>

          <p>Categories</p>
        </div>

        <div class="icon">
          <i class="fa fa-list"></i>
        </div>

        <a href="/admin/categories" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{$users_count}}</h3>

          <p>Users</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <a href="/admin/users" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{$products_count}}</h3>

          <p>Products</p>
        </div>
        <div class="icon">
          <i class="fa fa-product-hunt"></i>
        </div>
        <a href="/admin/products" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>

  </div>
@endsection
