@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Add Product</h3>
                    @include('partials.alerts')
                </div>

                <div class="box-body">
            
                    <form enctype="multipart/form-data" method = "post" action = "/admin/category/insert" id = "category-form"> 

                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="form-group has-feedback">
                                <input type="text" name = "name" class="form-control" placeholder="Name">
                        </div>

                        <div class="form-group has-feedback">
                        <select class = "form-control" name = "parent_id">
                            <option value = "0">Select Parent Category</option>
                            @foreach($main_categories as $category_details)
                                <option value = "{{$category_details->id}}">{{$category_details->name}}</option>
                            @endforeach
                        </select>    
                        </div>
                            
                        <div class="form-group has-feedback">
                            <input type = "file" name = "category_image">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                        </div>
                        
                    <form>
                </div>
            </div>
        </div>
    </div>    
@endsection