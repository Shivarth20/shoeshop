@extends('layouts.admin')

@section('content-header')
    <a href = "/admin/category/add"><button class = "btn btn-primary" style = "float:right;margin-bottom:10px;">Add</button></a>
@endsection


@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">

        <div class="box-header with-border">
            <h3 class="box-title">Categories List</h3>
        </div>

        <div class="box-body">
            @include('partials.alerts')
            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="example2" class="table table-bordered table-hover categories-table" role="grid" aria-describedby="example2_info">
                <thead>
                    <tr role="row">
                        <th>Id</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Parent</th>
                        <th>Created At</th>
                        <th>Last Updated</th>
                        <th class = "action_tab">Actions</th>
                    </tr>
                </thead>
            </table>    
            </div>
        </div>        
    </div>

</div>
@endsection