@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Edit Category</h3>
                    @include('partials.alerts')
                </div>
                
                <div class="box-body">                    

                        <form enctype="multipart/form-data" method = "post" action = "/admin/category/update" id = "category-form"> 

                            <input type = "hidden" name="_token" value="{{csrf_token()}}">
                            <input type = "hidden" name = "id" value = "{{$category_details->id}}">

                            <div class="form-group has-feedback">
                                <input type="text" name = "name" class="form-control" value = "{{$category_details->name}}" placeholder="Name">
                            </div>
                            <div class="form-group has-feedback">
                                <select class = "form-control" name = "parent_id">
                                    <option value = "0">Select Parent Category</option>
                                    @foreach($main_categories as $parent_category)
                                        <option value = "{{$parent_category->id}}"   {{$parent_category->id == $category_details->parent_id ? 'selected': '' }} >{{$parent_category->name}}</option>
                                    @endforeach
                                </select>    
                            </div>

                            <div class="form-group has-feedback">
                                <input type = "file" name = "category_image">
                                <img src = "{{$category_details->image_url}}" style = "width:150px;margin-top: 20px;">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                            </div>
                            
                        <form>
                </div>
            </div>
        </div>           
    </div>    
@endsection