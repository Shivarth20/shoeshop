@extends('layouts.admin')
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Edit Users Details</h3>
                    @include('partials.alerts')
                </div>

                <div class="box-body">
                 
                    <form method = "post" action = "/admin/user/update" id = "user-form"> 

                        <input type = "hidden" name="_token" value="{{csrf_token()}}">
                        <input type = "hidden" name = "id" value = "{{$user_details->id}}">

                        <div class = "row">
                            <div class = "col-xs-6">

                                <div class="form-group has-feedback">
                                    <input type="text" name = "name" class="form-control" value = "{{$user_details->name}}" placeholder="Name">
                                </div>

                                <div class="form-group has-feedback">
                                    <input type="tel" name = "mobile" class="form-control" placeholder="Slug" value = "{{$user_details->mobile}}">
                                </div>
                                
                            </div> 

                            <div class = "col-xs-6">

                                <div class="form-group has-feedback">
                                    <input type="email" name = "email" class="form-control" placeholder="Email" value = "{{$user_details->email}}">
                                </div>

                                <div class="form-group has-feedback">
                                    <select name = "role-id" class = "form-control">
                                        <option value = "1">Admin</option>
                                        <option value = "2">Customer</option>
                                    </select>
                                </div>

                            </div>
                        </div>    
                            

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                        </div>
                        
                    <form>
                </div> 
            
            </div>
        </div>
    </div>       
@endsection