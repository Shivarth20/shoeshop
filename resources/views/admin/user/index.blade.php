@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Users List</h3>
                </div>

            <div class="box-body">
                @include('partials.alerts')
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example2" class="table table-bordered table-hover users-table" role="grid" aria-describedby="example2_info">
                    <thead>
                        <tr role="row">
                            <th>Id</th>
                            <th>Name</th>
                            <th>Avatar</th>
                            <th>Email Id</th>
                            <th>Phone</th>
                            <th>Role</th>
                            <th>Created At</th>
                            <th>Last Updated</th>
                            <th class = "action_tab">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>    
                </div>
            </div>
        </div>
</div>        
@endsection 