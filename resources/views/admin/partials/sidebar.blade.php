<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li class = "{{(request()->is('admin')) || (request()->is('admin/dashboard')) ? 'active' : ''}}">
          <a href="/admin"><i class="fa fa-dashboard active"></i> <span>Dasboard</span></a>
        </li>
        
        <li class="treeview {{(request()->is('admin/users')) ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-users"></i> <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class = "{{(request()->is('admin/users')) ? 'active' : ''}}"><a href="/admin/users"><i class="fa fa-angle-right"></i> View Users</a></li>
          </ul>
        </li>

        <li class="treeview {{(request()->is('admin/categor*')) ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-list"></i> <span>Categories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class = "{{(request()->is('admin/categories')) ? 'active' : ''}}"><a href="/admin/categories"><i class="fa fa-angle-right"></i> View Categories</a></li>
            <li class = "{{(request()->is('admin/category/add')) ? 'active' : ''}}"><a href="/admin/category/add"><i class="fa fa-angle-right"></i> Add Category</a></li>
          </ul>
        </li>

        <li class="treeview {{(request()->is('admin/product*')) ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-product-hunt"></i> <span>Products</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class = "{{(request()->is('admin/products')) ? 'active' : ''}}"><a href="/admin/products"><i class="fa fa-angle-right"></i> View Products</a></li>
            <li class = "{{(request()->is('admin/product/add')) ? 'active' : ''}}"><a href="/admin/product/add"><i class="fa fa-angle-right"></i> Add Product</a></li>
          </ul>
        </li>

        <li class="treeview {{(request()->is('admin/orders')) ? 'active' : ''}}">
          <a href="#">
            <i class="fa fa-cart-arrow-down"></i> <span>Orders</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class = "{{(request()->is('admin/orders')) ? 'active' : ''}}"><a href="/admin/orders"><i class="fa fa-angle-right"></i> View Order</a></li>
          </ul>
        </li>

      </ul>
      
    </section>
  </aside>