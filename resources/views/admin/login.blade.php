
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="/css/admin.min.css">
  <link rel="stylesheet" href="/css/skins/_all-skins.min.css">

  
  <link rel="stylesheet" href="https://fontss.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="/admin"><b>{{config('app.name')}}</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <h3 class="login-box-msg">LogIn </h4>

    @include('partials.alerts')

    <form id = "login-form" action="/admin/login-form" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">

      <div class="form-group has-feedback">
        <input type="email" name = "email" class="form-control" placeholder="Email">
      </div>

      <div class="form-group has-feedback">
        <input type="password" type = "password" name = "password" class="form-control" placeholder="Password">
      </div>

      <div class="form-group">
          <button type="submit" class="btn btn-primary btn-flat">Sign In</button>
      </div>
      
    </form>


  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="js/jquery-2.1.1.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="/js/jquery.validate.min.js"></script>

<script>
  $('#login-form').validate();
</script>
</body>
</html>
