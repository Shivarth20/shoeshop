@extends('layouts.admin')

@section('content-header')
    <a href = "/admin/product/add"><button class = "btn btn-primary" style = "float:right;margin-bottom:10px;">Add</button></a>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">

            <div class="box-header with-border">
                <h3 class="box-title">Products List</h3>
            </div>

        <div class="box-body">
            @include('partials.alerts')
            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="example2" class="table table-bordered table-hover products-table" role="grid" aria-describedby="example2_info">
                <thead>
                    <tr role="row">
                        <th>Id</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th class = "action_tab">Actions</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>    
            </div>
        </div>        
    </div>
</div>        
@endsection