@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Edit Products</h3>
                    @include('partials.alerts')
                </div>

                <div class="box-body">
                 
                    <form method = "post" action = "/admin/product/update" id = "product-form"> 

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type = "hidden" name = "id" value = "{{$product_details->id}}">

                       <div class = "row">
                            <div class = "col-sm-6">
                                <div class="form-group has-feedback">
                                    <input type="text" name = "name" class="form-control" value = "{{$product_details->name}}" placeholder="Name">
                                </div>

                                <div class="form-group has-feedback">
                                    <select name = "parent_category" class = "form-control">
                                        <option></option>
                                        @foreach($parent_categories as $id => $category_name)
                                            <option {{$id == $product_details->category_id ? "selected" : "" }} value = "{{$id}}">{{$category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group has-feedback">
                                    <textarea rows = "4" name = "product_description" class="form-control" placeholder="Description">{{$product_details->description}}</textarea>
                                </div>
                            </div>

                            <div class = "col-sm-6">
                            </div>
                            
                        </div>        

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                        </div>
                        
                    <form>

                </div>

            </div>
        </div>    

    </div>    
@endsection