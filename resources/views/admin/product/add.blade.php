@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

            <div class="box-header with-border">
                <h3 class="box-title">Add Product</h3>
                @include('partials.alerts')
            </div>

                <div class="box-body">
                         
                    <form enctype="multipart/form-data" method = "post" action = "/admin/product/insert" id = "product-form"> 

                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <div class="form-group has-feedback">
                            <input type="text" name = "name" class="form-control" placeholder="Name">
                        </div>
                        
                        <div class="form-group has-feedback">
                            <select name = "parent_category" class = "form-control">
                                <option></option>
                                @foreach($parent_categories as $id => $category_name)
                                    <option value = "{{$id}}">{{$category_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group has-feedback">
                            <textarea rows = "4" name = "product_description" class="form-control" placeholder="Description"></textarea>
                        </div>

                        <div class="form-group has-feedback">
                            <input type = "file" name = "product_image[]" multiple>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                        </div>
                        
                    <form>

                </div>
            </div>
        </div>
    </div>
@endsection