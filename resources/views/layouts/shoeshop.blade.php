<!DOCTYPE html>

<html lang="en">
  <head>
  <title>Mo & Chi</title><meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="e-commerce site well design with responsive view." />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="stylesheet" href="{{asset('css/stylesheet.css')}}" />
  <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" media="screen" />
  <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

  <script src="{{asset('js/jquery-2.1.1.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('js/global.js')}}" type="text/javascript"></script>
  </head>

  <body class="account-register">
    <div class="preloader loader" style="display: block; background:#f2f2f2;"> <img src="image/loader.gif"  alt="#"/></div>
    <header>

      <!-- Include Navbar -->
      @include('shoeshop.partials.navtop')

      <div class="container">

        <!-- Include Navbar -->
        @include('shoeshop.partials.navheader')

        <!-- Include Navbar -->
        @include('shoeshop.partials.navmenu')

      </div>
    </header>
    <div class = "container">
      @yield('content')
    </div>
    <hr>
    <footer>

      <!-- Include Footer -->
      @include('shoeshop.partials.footer')
      <a id="scrollup">Scroll</a> 

    </footer>

    <div class="footer-bottom">
      <div class="container">
        <div class="copyright">Powered By &nbsp;<a class="yourstore" href="http://www.lionode.com/">lionode &copy; 2017 </a> </div>
        <div class="footer-bottom-cms">
          <div class="footer-payment">
            <ul>
              <li class="mastero"><a href="#"><img alt="" src="image/payment/mastero.jpg"></a></li>
              <li class="visa"><a href="#"><img alt="" src="image/payment/visa.jpg"></a></li>
              <li class="currus"><a href="#"><img alt="" src="image/payment/currus.jpg"></a></li>
              <li class="discover"><a href="#"><img alt="" src="image/payment/discover.jpg"></a></li>
              <li class="bank"><a href="#"><img alt="" src="image/payment/bank.jpg"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
