<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{config('app.name')}} Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="/css/admin.min.css">
  <link rel="stylesheet" href="/css/styleadmin.css">

  <link rel="stylesheet" href="/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

</head>
<meta name="csrf-token" content="{{ csrf_token() }}">

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        @include('admin.partials.header')

        <!-- Sidebar  -->
        @include('admin.partials.sidebar')
        
        <div class="content-wrapper" style="min-height: 1136px;">
           
            <section class="content-header">
                @yield('content-header')
            </section>
        
            <!-- Page Content  -->
            <section class="content">
                @yield('content')
            </section>

        </div>

        @include('admin.partials.footer')


    </div>

<script src = "/js/jquery.min.js">
<script src="/bootstrap/js/bootstrap.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/js/bootstrap/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src = "https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.6.0/dist/alpine.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.material.min.js"></script>
<script src = "/js/global-admin.js"></script>
<script src="/js/admin.min.js"></script>    

</body>
</html>
